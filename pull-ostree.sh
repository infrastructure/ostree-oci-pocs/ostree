#!/bin/bash

set -ex

if ostree --version | grep -q sign-ed25519; then
  sign_verify=--sign-verify=ed25519=inline:wZNzf9ayPjihOi3arbB/m5gn5DLD+669lnbKGOAvcHM=
  prefix=ostree-remote-image:apertis
else
  prefix=ostree-unverified-image
fi

mkdir -p pull-repo
ostree --repo=pull-repo init --mode=bare-user
ostree --repo=pull-repo remote add --if-not-exists --no-gpg-verify $sign_verify \
  apertis ''
ostree-ext-cli container unencapsulate --repo=pull-repo \
  --write-ref=apertis/v2024dev1/arm64-uboot/fixedfunction \
  $prefix:docker://registry.gitlab.apertis.org/infrastructure/ostree-oci-pocs/ostree/arm64-uboot/fixedfunction:latest
