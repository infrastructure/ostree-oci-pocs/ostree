# ostree-oci-test

This repo contains a simple example of working with bootable OSTree commits from
GitLab's container registry.

- `push-ostree.sh` downloads the arm64 fixedfunction Apertis and uploads it to
  GitLab container registry.
  - The `ostree.bootable` metadata needs to be set to be able to later pull the
    image, but Apertis's image builds (incorrectly) don't set it, so it's set
    in the script manually.
- `pull-ostree.sh` downloads the container registry image and saves it to a
  local bare-user repository.
  - This adds a remote with an empty URL to hold the needed signature info.
