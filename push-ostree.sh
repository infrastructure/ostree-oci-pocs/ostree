#!/bin/bash

set -ex

if ostree --version | grep -q sign-ed25519; then
  sign_verify=--sign-verify=ed25519=inline:wZNzf9ayPjihOi3arbB/m5gn5DLD+669lnbKGOAvcHM=
fi

mkdir -p push-repo
ostree --repo=push-repo init --mode=archive-z2
ostree --repo=push-repo remote add --if-not-exists --no-gpg-verify $sign_verify \
  apertis https://images.apertis.org/ostree/repo
ostree --repo=push-repo pull apertis apertis/v2024dev1/arm64-uboot/fixedfunction
ostree-ext-cli container encapsulate --repo=push-repo --compression-fast \
  -l ostree.bootable=true \
  apertis/v2024dev1/arm64-uboot/fixedfunction \
  docker://registry.gitlab.apertis.org/infrastructure/ostree-oci-pocs/ostree/arm64-uboot/fixedfunction:latest
